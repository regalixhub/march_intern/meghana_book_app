export class BookList {
    title: string;
    author: string;
    ISBN: number;
    description: string;
    constructor(title, author, ISBN, description) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.description = description;
    }
}
