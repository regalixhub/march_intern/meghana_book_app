import { Component } from '@angular/core';
import { BookList } from './book-list';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  books: Array<BookList>;
  constructor() {
      this.books = [];
  }
  
  addBook(title, author, ISBN, description){
    const book = new BookList(title, author, ISBN, description);
    this.books.push(book);
 }

removeBook(book) {
    const index = this.books.indexOf(book);
    this.books.splice(index, 1);
}

updateBook(book)
{
        let updatedBook;
        const updateBookName = prompt('Please enter the book name:', book.title);
        const updateAuthor = prompt('Please enter the author name:', book.author);
        const updateIsbn = prompt('Please enter the isbn number:', book.isbn);
        const updateDescription = prompt('Please enter book description:', book.description);
        updatedBook = new BookList(updateBookName, updateAuthor, updateIsbn, updateDescription);
        const index = this.books.indexOf(book);
        this.books.splice(index, 1, updatedBook);
}
}
